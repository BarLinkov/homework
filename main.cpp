#include "Menu.h"
#include "Shape.h"

#define EXIT 2
#define FIRST_CHOICE 0
#define SECOND_CHOICE 1

void main()
{
	Menu m;
	Shape * s;
	Point * a = new Point(50, 100);
	Point * b = new Point(200, 250);
	Point * c = new Point(150, 200);
	Point * toMove = new Point(100, 200);

	do
	{
		m.printMenu();
		if (m.getChoice == FIRST_CHOICE) // creating new shape
		{
			m.chooseNewShape();
			if (m.getShapeChoice() == 0) //creating new circle
			{
				s = new Circle(*a, 25, "Circle", "ciri");
			}
			if (m.getShapeChoice() == 1) //creating new Arrow
			{
				s = new Arrow(*a, *b, "Arrow", "arri");
			}
			if (m.getShapeChoice() == 2)
			{
				try
				{
					s = new Triangle(*a, *b, *c, "Triangle", "tri");
				}
				catch (const std::exception)
				{
					std::cout << "wrong arguments!" << std::endl;
				}
			}
			if (m.getShapeChoice() == 3)
			{
				try
				{
					s = new myShapes::Rectangle(*a, 40, 60, "Rectangle", "rect");
				}
				catch (const std::exception)
				{
					std::cout << "wrong arguments!" << std::endl;
				}
			}
		}
	}
