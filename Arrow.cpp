#include "Arrow.h"



void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}

Arrow::Arrow(const Point & a, const Point & b, const string & type, const string & name) : Shape(type, name) //c'tor og Arrow
{
	this->_p1 = a;
	this->_p2 = b;
	this->_type = type;
	this->_name = name;
}

double Arrow::getArea() const 
{
	double area = 0;
	return area;
}

double Arrow::getPerimeter() const 
{
	double perimeter = this->_p1.distance(_p2);
	return perimeter;
}

void Arrow::move(const Point & other)
{
	clearDraw(disp, board);
	this->_p1 = other + _p1;
	this->_p2 = other + _p2;
	draw(disp, board);
}


