#include "Rectangle.h"




void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


double myShapes::Rectangle::getArea() const //the function calculates the area of the rectangle and returns it
{
	double area = _length * _width;
	return area;
}

void myShapes::Rectangle::move(const Point & other)
{
	clearDraw(disp, board);
	this->_p1 += other;
	draw(disp, board);
}


myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name) :Polygon(type, name)
{//c'tor of Rectangle
	if (width != 0 && length != 0)
	{
		this->_p1 = a;
		this->_width = width;
		this->_length = length;
		this->_name = name;
		this->_type = type;
	}
	else
	{
		throw exception("invalid_arguments");
	}
}


