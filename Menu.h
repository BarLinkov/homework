#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();
	void printMenu();
	void chooseActWithShape();
	void chooseNewShape();
	int getAct();
	int getChoice();
	int getShapeChoice();
	cimg_library::CImg<unsigned char>* getBoard();
	cimg_library::CImgDisplay* getDisp();
	// more functions..

private:
	int _choice;
	int _action;
	int _shapeChoice;
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};



