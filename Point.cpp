#include "Point.h"
#include <cmath>

Point::Point(double x, double y) 
{
	this->_x = x;
	this->_y = y;
}

Point Point::operator+(const Point & other) const 
{
	Point point(_x, _y);
	point._x = this->getX() + other.getX();
	point._y = this->getY() + other.getY();
	return point;
}

Point & Point::operator+=(const Point & other) 
{
	this->_x = _x + other._x;
	this->_y = _y + other._y;
	return *this;
}

double Point::distance(const Point & other) const 
	double distance = 0;
	double disX = this->getX() - other.getX();
	double disY = this->getY() - other.getY();
	disX = pow(disX, 2);
	disY = pow(disY, 2);
	distance = sqrt(disX + disY);
	return distance;
}

double Point::getX() const 
{
	return _x;
}

double Point::getY() const /
{
	return _y;
}

