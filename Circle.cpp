#include "Circle.h"


void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
}

Circle::Circle(const Point & center, double radius, const string & type, const string & name) : Shape(name, type), _center(center), _radius(radius) //thie c'tor function
{

}

Circle::~Circle() 
{

}

void Circle::move(const Point & other)
{
	clearDraw(disp, board);
	_center += other;
	draw(disp, board);

}

double Circle::getArea() const // the function calculates the area of a circle and returns it
{
	double area = PI * _radius * _radius;
	return area;
}

double Circle::getPerimeter() const // the function calculates the perimeter of a circle and returns it
{
	double perimeter = 2 * PI * _radius;
	return perimeter;
}

const Point & Circle::getCenter() const // returning the circle's center
{
	return _center;
}



