#include "Shape.h"
#include <iostream>
#include <string>
using namespace std;


void Shape::printDetails() const
{
	cout << "The shape's Type: " << getType() << endl;
	cout << "The shape's Name: " << getName() << endl;
	cout << "The Shape's Area: " << this->getArea() << endl;
	cout << "The Shape's perimeter: " << this->getPerimeter() << endl;
}
string Shape::getType() const
{
	return _type;
}
string Shape::getName() const
{
	return _name;
}

Shape::Shape(const string& name, const string& type) //the c'tor function
{
	_name = name;
	_type = type;
}