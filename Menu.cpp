#include "Menu.h"


Menu::Menu()
{
	_board = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::printMenu() // prints the menu and getting the user choice
{
	int choice;
	std::cout << "Enter 0 to add a new shape." << endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << endl;
	std::cout << "Enter 2 to exit." << endl;
	std::cin >> choice;
	this->_choice = choice;
}

void Menu::chooseActWithShape() // prints the different actions you can do with your shape and getting the user input
{
	int act;
	std::cout << "Enter 0 to move the shape " << endl;
	std::cout << "Enter 1 to get its details." << endl;
	std::cout << "Enter 2 to remove the shape." << endl;
	std::cin >> act;
	this->_action = act;
}

void Menu::chooseNewShape() // prints the exists shapes and getting the user input
{
	int shape;
	std::cout << "Enter 0 to add a circle." << endl;
	std::cout << "Enter 1 to add an arrow." << endl;
	std::cout << "Enter 2 to add a triangle." << endl;
	std::cout << "Enter 3 to add a rectangle." << endl;
	std::cin >> shape;
	this->_shapeChoice = shape;
}

cimg_library::CImgDisplay * Menu::getDisp() //returning the disp
{
	return this->_disp;
}

cimg_library::CImg<unsigned char>* Menu::getBoard() // returning the board
{
	return this->_board;
}

int Menu::getChoice() // returning the chosen choice in the main menu
{
	return this->_choice;
}

int Menu::getAct() // returning the chosen action with the shape
{
	return this->_action;
}

int Menu::getShapeChoice() // returning the chosen choice of type of shape 
{
	return this->_shapeChoice;
}